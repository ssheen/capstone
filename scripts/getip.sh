# Finds and lists all IP's in a newline delimited file with the given IP/subnet
# Diffs the results with a file 'oldfilename' to create the difference in ips
# and saves that to 'newfilename'

# IP and Subnet
network=10.0.0.0/24

# File names to save to
filename=ip.txt
oldfilename=oldip.txt
newfilename=newip.txt

if [ -f $oldfilename ]; then
   touch $oldfilename
fi

if [ -f $newfilename ]; then
   touch $newfilename
fi

nmap -sP -n $network | awk '/scan report for/ {$1=$2=$3=$4=""; print $0}' > __temp__.txt

if [ -f $filename ]; then
   rm $filename
fi

while read line; do
   echo $line >> $filename
done < __temp__.txt

rm __temp__.txt

sort $filename -o $filename
sort $oldfilename -o $oldfilename
comm -32 $filename $oldfilename > $newfilename

wc $filename





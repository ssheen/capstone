# check if it exists
sudo rm /etc/ntp.conf

echo Installing packages...
echo ubuntu | sudo -S dpkg -i /home/ubuntu/Desktop/packages/*.deb
echo Installing packages... done

echo Mounting NFS directories...
echo ubuntu | sudo -S mount 10.0.0.1:/home/ubuntu/Desktop/jetson /home/ubuntu/Desktop/jetson
echo ubuntu | sudo -S mount 10.0.0.1:/usr/local/openmpi-1.8.3 /usr/local/openmpi-1.8.3
echo Mounting NFS directories... done

echo Installing other packages...
echo ubuntu | sudo -S dpkg -i /home/ubuntu/Desktop/jetson/packages/*.deb
echo Installing other packages... done

echo Copying Ganglia config...
echo ubuntu | sudo -S cp /home/ubuntu/Desktop/gmond.conf /etc/ganglia/.
echo Copying Ganglia config... done

echo Copying bashrc...
echo ubuntu | sudo -S cp /home/ubuntu/Desktop/bash.bashrc /etc/.
echo Copying bashrc... done

echo Setting up startup scripts...
echo ubuntu | sudo -S cp /home/ubuntu/Desktop/startservices /home/ubuntu/Desktop/.
echo ubuntu | sudo -S cp -r /home/ubuntu/Desktop/startup/* /etc/NetworkManager/dispatcher.d/.
echo ubuntu | sudo -S cp /home/ubuntu/Desktop/ntp.conf /etc/.
echo ubuntu | sudo -S service ntp restart
echo ubuntu | sudo -S ntpdate -u 10.0.0.1
echo Setting up startup scripts... done

# Makes all the folders for nfs/packages/etc

# Does not check if they exist so errors will pop up if they do
echo Creating directories...
mkdir /home/ubuntu/Desktop/packages
mkdir /home/ubuntu/Desktop/jetson
mkdir /home/ubuntu/Desktop/startup
echo ubuntu | sudo -S mkdir /usr/local/openmpi-1.8.3
mkdir ~/.ssh
echo Creating directories... done


# Set up permissions so nfs works
echo Setting up file permissions...
echo ubuntu | sudo -S chmod 700 /home/ubuntu/Desktop/jetson
echo ubuntu | sudo -S chmod 755 ~/.ssh
echo ubuntu | sudo -S chmod 755 ~/../ubuntu
echo Setting up file permissions... done

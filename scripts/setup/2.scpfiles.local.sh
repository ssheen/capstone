echo Setting up RSA key...
sshpass -p 'ubuntu' scp ~/.ssh/* ubuntu@$1:~/.ssh
echo Setting up RSA key... done

echo Copying NFS packages...
sshpass -p 'ubuntu' scp /home/ubuntu/Desktop/scripts/setup/packages/* ubuntu@$1:/home/ubuntu/Desktop/packages/.
echo Copying NFS packages... done

echo Copying bashrc...
sshpass -p 'ubuntu' scp /etc/bash.bashrc ubuntu@$1:/home/ubuntu/Desktop/.
echo Copying bashrc... done

echo Setting up gmond...
sshpass -p 'ubuntu' scp /home/ubuntu/Desktop/scripts/setup/gmond.conf ubuntu@$1:/home/ubuntu/Desktop/.
echo Setting up gmond... done

echo Setting up ntp...
sshpass -p 'ubuntu'scp /home/ubuntu/Desktop/scripts/setup/ntp.conf ubuntu@$1:/home/ubuntu/Desktop/.
echo Setting up ntp... done

echo Setting up startup scripts...
sshpass -p 'ubuntu' scp -r /home/ubuntu/Desktop/scripts/setup/startup/* ubuntu@$1:/home/ubuntu/Desktop/startup/
sshpass -p 'ubuntu' scp /home/ubuntu/Desktop/scripts/setup/startservices ubuntu@$1:/home/ubuntu/Desktop/.
echo Setting up startup scripts... done

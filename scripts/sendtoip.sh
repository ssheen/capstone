# sends a script to all IP's
# 1st command line argument is the script to send/run

# file to read ip's from (newline delimited ips)
filename=ip.txt

while read ip; do
   if [ $ip != "10.0.0.1" ]; then
      sshpass -p 'ubuntu' ssh ubuntu@$ip 'bash -s' < $1 $2&
   fi
done < $filename


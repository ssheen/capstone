# takes a commandline argument filename that should be an IP file delimited
# by newlines and prints out a line that makes mpirun easier (copy psate stuff)

number=$(wc -l $1 | awk '{print $1}')
iplist=$(awk '{print $1}' < $1 | paste -s -d,)

echo $iplist
echo $number
echo mpirun -np $number -H $iplist 

# Sets up all ssh keys and copies the known_hosts from the main board to 
# all other boards with the given list of IPs

# Filename is a file with IPs delimited by newlines
filename=ip.txt

while read ip; do
   if [ $ip != "10.0.0.1" ]; then
      echo setting up $ip
      ssh-keygen -R $ip
      ssh-keyscan -H $ip >> ~/.ssh/known_hosts
   fi 
done < $filename

while read ip; do
   if [ $ip != "10.0.0.1" ]; then
      echo copying ssh
      sshpass -p 'ubuntu' scp ~/.ssh/* ubuntu@$ip:~/.ssh/ &
   fi
done < $filename

#while read ip; do
#   if [ $ip != "10.0.0.1" ]; then
#      echo copying authorized keys
#      echo ubuntu | sudo -S ssh-copy-id ubuntu@$ip
#   fi
#done < $filename

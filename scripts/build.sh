# Runs all script in loc. All scripts need to end in either .sh or .sh.local
# .sh runs the script on the target machine
# .sh.local runs the script on host machine (useful for scp stuff, key stuff)

# The scripts will be ran on all IP's listed in a newline delimited list of IP's
# the ./getip script will do this and write to 2 files ip.txt and newip.txt
# newip.txt contains all ips that are not in oldip.txt
# ip.txt contains all ips

loc=setup/*
#while read ip; do
#   echo setting up ssh keys first
#   ssh-keygen -R $ip
#   ssh-keyscan -H $ip >> ~/.ssh/known_hosts
#done < newip.txt

# .local.sh scripts are ran first in the background
# BUGS 
# 1. if the network is slow some scp commands will not complete
# before running the next step of commands that might rely on them
# Possible Fix
# There are a few better solutions but the I just used a sleep

for scriptName in $loc;
do
   if [[ "$scriptName" == *".local.sh" ]]; then
      while read ip; do
         echo running $scriptName 
         ./$scriptName $ip &  
         proc=$!
      done < newip.txt
      wait "proc"
      sleep 2
   elif [[ "$scriptName" == *".sh" ]]; then
      while read ip; do
         echo running $scriptName on $ip
         sshpass -p 'ubuntu' ssh ubuntu@$ip 'bash -s' < $scriptName &
         proc=$!
      done < newip.txt
      wait "proc"
      sleep 2
   fi
   echo all done
done
